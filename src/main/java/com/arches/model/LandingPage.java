package com.arches.model;

public class LandingPage 
{
	private Long id;
	private String landingPagePath;
	private Double sessions;
	private Double newUsers;
	private Double percentNewSessions;
	private Double bounceRate;
	private Double pageViewsPerSession;
	private Double avgSessionDuration;
	private String entryTZ;
	private String entryDate;
	private Long accountId;
	private Long property;
	private String actualDate;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getActualDate() {
		return actualDate;
	}
	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}
	public String getLandingPagePath() {
		return landingPagePath;
	}
	public void setLandingPagePath(String landingPagePath) {
		this.landingPagePath = landingPagePath;
	}
	public Double getSessions() {
		return sessions;
	}
	public void setSessions(Double sessions) {
		this.sessions = sessions;
	}
	public Double getNewUsers() {
		return newUsers;
	}
	public void setNewUsers(Double newUsers) {
		this.newUsers = newUsers;
	}
	public Double getPercentNewSessions() {
		return percentNewSessions;
	}
	public void setPercentNewSessions(Double percentNewSessions) {
		this.percentNewSessions = percentNewSessions;
	}
	public Double getBounceRate() {
		return bounceRate;
	}
	public void setBounceRate(Double bounceRate) {
		this.bounceRate = bounceRate;
	}
	public Double getPageViewsPerSession() {
		return pageViewsPerSession;
	}
	public void setPageViewsPerSession(Double pageViewsPerSession) {
		this.pageViewsPerSession = pageViewsPerSession;
	}
	public Double getAvgSessionDuration() {
		return avgSessionDuration;
	}
	public void setAvgSessionDuration(Double avgSessionDuration) {
		this.avgSessionDuration = avgSessionDuration;
	}
	public String getEntryTZ() {
		return entryTZ;
	}
	public void setEntryTZ(String entryTZ) {
		this.entryTZ = entryTZ;
	}
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public Long getProperty() {
		return property;
	}
	public void setProperty(Long property) {
		this.property = property;
	}
	
	
}
