package com.arches.model;

public class SiteSpeed 
{
	private Long id;
	private Double pageLoadTime;
	private Double AvgPageLoadTime;
	private String entryTZ;
	private String entryDate;
	private Long property;
	private Long accountId;
	private String actualDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getActualDate() {
		return actualDate;
	}
	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}
	public Double getPageLoadTime() {
		return pageLoadTime;
	}
	public void setPageLoadTime(Double pageLoadTime) {
		this.pageLoadTime = pageLoadTime;
	}
	public Double getAvgPageLoadTime() {
		return AvgPageLoadTime;
	}
	public void setAvgPageLoadTime(Double avgPageLoadTime) {
		AvgPageLoadTime = avgPageLoadTime;
	}
	public String getEntryTZ() {
		return entryTZ;
	}
	public void setEntryTZ(String entryTZ) {
		this.entryTZ = entryTZ;
	}
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	public Long getProperty() {
		return property;
	}
	public void setProperty(Long property) {
		this.property = property;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	
}
