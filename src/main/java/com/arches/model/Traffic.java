package com.arches.model;

public class Traffic {

	private Long id;
	private String referralPath;
	private Double users;
	private String campaign;
	private String source;
	private String medium;
	private String entryTZ;
	private String entryDate;
	private Long accountId;
	private Long property;
	private String actualDate;
	
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	
	public Double getUsers() {
		return users;
	}
	public void setUsers(Double users) {
		this.users = users;
	}
	public String getActualDate() {
		return actualDate;
	}
	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}
	public Long getProperty() {
		return property;
	}
	public void setProperty(Long property) {
		this.property = property;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getReferralPath() {
		return referralPath;
	}
	public void setReferralPath(String referralPath) {
		this.referralPath = referralPath;
	}
	public String getCampaign() {
		return campaign;
	}
	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}
	public String getEntryTZ() {
		return entryTZ;
	}
	public void setEntryTZ(String entryTZ) {
		this.entryTZ = entryTZ;
	}
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	
}
