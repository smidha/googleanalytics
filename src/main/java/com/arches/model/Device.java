package com.arches.model;

public class Device 
{
	private Long id;
	private String deviceCategory;
	private Double users;
	private String entryTZ;
	private String entryDate;
	private Long accountId;
	private Long property;
	private String actualDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getActualDate() {
		return actualDate;
	}
	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}
	public Double getUsers() {
		return users;
	}
	public void setUsers(Double users) {
		this.users = users;
	}
	public String getDeviceCategory() {
		return deviceCategory;
	}
	public void setDeviceCategory(String deviceCategory) {
		this.deviceCategory = deviceCategory;
	}
	public String getEntryTZ() {
		return entryTZ;
	}
	public void setEntryTZ(String entryTZ) {
		this.entryTZ = entryTZ;
	}
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public Long getProperty() {
		return property;
	}
	public void setProperty(Long property) {
		this.property = property;
	}
	
	
}
