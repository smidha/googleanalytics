package com.arches.model;

public class Property 
{
	private Long id;
	private String name;
	private Long accountId;
	private String gaAccountId;
	private String gaPropertyId;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public String getGaAccountId() {
		return gaAccountId;
	}
	public void setGaAccountId(String gaAccountId) {
		this.gaAccountId = gaAccountId;
	}
	public String getGaPropertyId() {
		return gaPropertyId;
	}
	public void setGaPropertyId(String gaPropertyId) {
		this.gaPropertyId = gaPropertyId;
	}
	
}
