package com.arches.model;

import javax.persistence.Id;

public class Sessions
{
	@Id
	private Long id;
	private Double sessions;
	private Double bounces;
	private Double bounceRate;
	private String entryTZ;
	private String entryDate;
	private Long accountId;
	private Long property;
	private String actualDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) 
	{
		this.id = id;
	}
	
	public String getActualDate() {
		return actualDate;
	}
	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}
	public Long getAccountId()
	{
		return accountId;
	}
	public void setAccountId(Long accountId)
	{
		this.accountId = accountId;
	}
	public Long getProperty() {
		return property;
	}
	public void setProperty(Long property) {
		this.property = property;
	}
	public Double getSessions() {
		return sessions;
	}
	public void setSessions(Double sessions) {
		this.sessions = sessions;
	}
	public Double getBounces() {
		return bounces;
	}
	public void setBounces(Double bounces) {
		this.bounces = bounces;
	}
	public Double getBounceRate() {
		return bounceRate;
	}
	public void setBounceRate(Double bounceRate) {
		this.bounceRate = bounceRate;
	}
	public String getEntryTZ() {
		return entryTZ;
	}
	public void setEntryTZ(String entryTZ) {
		this.entryTZ = entryTZ;
	}
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	
	
}
