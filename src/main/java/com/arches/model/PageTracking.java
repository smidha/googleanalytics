package com.arches.model;

public class PageTracking 
{
	private Long id;
	private String pagePath;
	private Double pageViews;
	private Double uniquePageViews;
	private Double timeOnPage;
	private Double avgTimeOnPage;
	private String entryTZ;
	private String entryDate;
	private Long accountId;
	private Long property;
	private String actualDate;

	public String getEntryTZ() {
		return entryTZ;
	}
	public void setEntryTZ(String entryTZ) {
		this.entryTZ = entryTZ;
	}
	
	public String getActualDate() {
		return actualDate;
	}
	public void setActualDate(String actualDate) {
		this.actualDate = actualDate;
	}
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public Long getProperty() {
		return property;
	}
	public void setProperty(Long property) {
		this.property = property;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPagePath() {
		return pagePath;
	}
	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}
	public Double getPageViews() {
		return pageViews;
	}
	public void setPageViews(Double pageViews) {
		this.pageViews = pageViews;
	}
	public Double getUniquePageViews() {
		return uniquePageViews;
	}
	public void setUniquePageViews(Double uniquePageViews) {
		this.uniquePageViews = uniquePageViews;
	}
	public Double getTimeOnPage() {
		return timeOnPage;
	}
	public void setTimeOnPage(Double timeOnPage) {
		this.timeOnPage = timeOnPage;
	}
	public Double getAvgTimeOnPage() {
		return avgTimeOnPage;
	}
	public void setAvgTimeOnPage(Double avgTimeOnPage) {
		this.avgTimeOnPage = avgTimeOnPage;
	}
	
}
