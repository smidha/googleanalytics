package com.arches.googleanalytics;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.Analytics.Data.Realtime.Get;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.RealtimeData;
import com.google.api.services.analytics.model.RealtimeData.ColumnHeaders;
import com.google.api.services.analytics.model.RealtimeData.ProfileInfo;
import com.google.api.services.analytics.model.RealtimeData.Query;

public class RealTime {
	  private static String APPLICATION_NAME = "cayston demo";
	  private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
	  private static final String KEY_FILE_LOCATION = "D:/Users/smidha/sandbox/googleanalytics/src/main/resources/cayston demo-3e565693a3e6.p12";
	  private static final String SERVICE_ACCOUNT_EMAIL = "caystondev@cayston-demo.iam.gserviceaccount.com";
	
	public static void main(String[] args) throws Exception
	{
		System.out.println("starting..");
	      Analytics analytics = initializeAnalytics();


	Get realtimeRequest = analytics.data().realtime()
		    .get("ga:124156732",
		         "rt:activeUsers")
		    .setDimensions("rt:browser");

		try {
		  RealtimeData realtimeData = realtimeRequest.execute();

			RealTime rtime=new RealTime();
			rtime.printRealtimeReport(realtimeData);
		  // Success.

		} catch (GoogleJsonResponseException e) {
		  // Catch API specific errors.
		 e.printStackTrace();

		} catch (IOException e) {
		  // Catch general parsing network errors.
		  e.printStackTrace();
		}
	}

	  private static Analytics initializeAnalytics() throws Exception {
	    // Initializes an authorized analytics service object.

	    // Construct a GoogleCredential object with the service account email
	    // and p12 file downloaded from the developer console.
	    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	    GoogleCredential credential = new GoogleCredential.Builder()
	        .setTransport(httpTransport)
	        .setJsonFactory(JSON_FACTORY)
	        .setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
	        .setServiceAccountPrivateKeyFromP12File(new File(KEY_FILE_LOCATION))
	        .setServiceAccountScopes(AnalyticsScopes.all())
	        .build();

	    // Construct the Analytics service object.
	    return new Analytics.Builder(httpTransport, JSON_FACTORY, credential)
	        .setApplicationName(APPLICATION_NAME).build();
	  }


		/**
		 * 2. Print out the Real-Time Data
		 * The components of the report can be printed out as follows:
		 */

		private void printRealtimeReport(RealtimeData realtimeData) {
		  System.out.println();
		  System.out.println("Response:");
		  System.out.println("ID:" + realtimeData.getId());
		  System.out.println("realtimeData Kind: " + realtimeData.getKind());
		  System.out.println();

		  printQueryInfo(realtimeData.getQuery());
		  printProfileInfo(realtimeData.getProfileInfo());
		  printPaginationInfo(realtimeData);
		  printDataTable(realtimeData);
		}

		private void printQueryInfo(Query query) {
		  System.out.println("Query Info:");
		  System.out.println("Ids: " + query.getIds());
		  System.out.println("Metrics: " + query.getMetrics());
		  System.out.println("Dimensions: " + query.getDimensions());
		  System.out.println("Sort: " + query.getSort());
		  System.out.println("Filters: " + query.getFilters());
		  System.out.println("Max results: " + query.getMaxResults());
		  System.out.println();
		}

		private void printProfileInfo(ProfileInfo profileInfo) {
		  System.out.println("Info:");
		  System.out.println("Account ID:" + profileInfo.getAccountId());
		  System.out.println("Web Property ID:" + profileInfo.getWebPropertyId());
		  System.out.println("Profile ID:" + profileInfo.getProfileId());
		  System.out.println("Profile Name:" + profileInfo.getProfileName());
		  System.out.println("Table Id:" + profileInfo.getTableId());
		  System.out.println();
		}
		 
		private void printPaginationInfo(RealtimeData realtimeData) {
		  System.out.println("Pagination info:");
		  System.out.println("Self link: " + realtimeData.getSelfLink());
		  System.out.println("Total Results: " + realtimeData.getTotalResults());
		  System.out.println();
		}

		private void printDataTable(RealtimeData realtimeData) {
		  if (realtimeData.getTotalResults() > 0) {
		    System.out.println("Data Table:");
		    for (ColumnHeaders header : realtimeData.getColumnHeaders()) {
		      System.out.format("%-32s", header.getName() + '(' + header.getDataType() + ')');
		    }
		    System.out.println();
		    for (List<String> row : realtimeData.getRows()) {
		      for (String element : row) {
		        System.out.format("%-32s", element);
		      }
		      System.out.println();
		    }
		  } else {
		    System.out.println("No data");
		  }
		}
}
