package com.arches.googleanalytics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.arches.model.Device;
import com.arches.model.GeoNetwork;
import com.arches.model.LandingPage;
import com.arches.model.PageTracking;
import com.arches.model.Platform;
import com.arches.model.Property;
import com.arches.model.Sessions;
import com.arches.model.SiteSpeed;
import com.arches.model.Traffic;
import com.arches.model.Users;
import com.google.api.services.analytics.model.GaData;

@Repository
@Transactional

public class DaoImpl  {
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	public void saveUsersData(Users user)
	{
		System.out.println("saving user...");
		Serializable result=getSessionFactory().getCurrentSession().save(user);
		//Serializable result=getHibernateTemplate().save(user);
	
	}
	public void saveSessionsData(Sessions session)
	{
		System.out.println("saving sessions...");
		Serializable result=getSessionFactory().getCurrentSession().save(session);
		//Serializable result=getHibernateTemplate().save(user);
	
	}
	public void saveTrafficData(Traffic traffic)
	{
		System.out.println("saving trafficData...");
		Serializable result=getSessionFactory().getCurrentSession().save(traffic);
		//Serializable result=getHibernateTemplate().save(user);
	
	}
	
	public void saveGeoNetworkData(GeoNetwork geoNetwork)
	{
		System.out.println("saving geo network Data...");
		Serializable result=getSessionFactory().getCurrentSession().save(geoNetwork);
		
		
	}
	public void saveSiteSpeedData(SiteSpeed siteSpeed)
	{
		System.out.println("saving siteSpeed...");
		Serializable result=getSessionFactory().getCurrentSession().save(siteSpeed);
		//Serializable result=getHibernateTemplate().save(user);
	
	}
	
	public void savePlatformData(Platform platform)
	{
		System.out.println("saving platform...");
		Serializable result=getSessionFactory().getCurrentSession().save(platform);
		//Serializable result=getHibernateTemplate().save(user);
	
	}
	public void saveDeviceData(Device device)
	{
		System.out.println("saving device...");
		Serializable result=getSessionFactory().getCurrentSession().save(device);
		//Serializable result=getHibernateTemplate().save(user);
	
	}

	public void savePageTrackingData(PageTracking pageTracking)
	{
		System.out.println("saving page tracking...");
		Serializable result=getSessionFactory().getCurrentSession().save(pageTracking);
		//Serializable result=getHibernateTemplate().save(user);
	
	}

	public void saveLandingPageData(LandingPage landingPage)
	{
		System.out.println("saving landing page tracking...");
		Serializable result=getSessionFactory().getCurrentSession().save(landingPage);
		//Serializable result=getHibernateTemplate().save(user);
	
	}

	public String[] getTableAccountIDAndPropertyName(String gaAccountID,String gaPropertyId)
	{
		
		String[] tableAccountAndProperty=new String[2];
		Query hibernateQuery=getSessionFactory().getCurrentSession().createQuery("from Property where gaAccountId=:gaAccountIdParam and gaPropertyId=:gaPropertyIdParam");
		
		hibernateQuery.setParameter("gaAccountIdParam",gaAccountID);
		hibernateQuery.setParameter("gaPropertyIdParam", gaPropertyId);
		List<Property> propertyList=	hibernateQuery.list();
		if(propertyList!=null && !propertyList.isEmpty())
		{
			Iterator<Property> propertyIterator=propertyList.iterator();
			if(propertyIterator.hasNext())
			{
				Property property=propertyIterator.next();
				tableAccountAndProperty[0]=Long.toString(property.getAccountId());
				tableAccountAndProperty[1]=Long.toString(property.getId());
			}
			else
			{
				tableAccountAndProperty=null;
			}
		}
		else
		{
			tableAccountAndProperty=null;
		}
		
		return tableAccountAndProperty;
	}
	
	
	
}
