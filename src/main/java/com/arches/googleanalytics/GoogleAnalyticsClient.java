package com.arches.googleanalytics;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;

import org.apache.commons.collections4.map.MultiValueMap;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.AnalyticsScopes;
import com.google.api.services.analytics.model.Accounts;
import com.google.api.services.analytics.model.GaData;
import com.google.api.services.analytics.model.Profiles;
import com.google.api.services.analytics.model.Webproperties;

public class GoogleAnalyticsClient {

	  private static String APPLICATION_NAME ;
	  private static  JsonFactory JSON_FACTORY = null;
	  private static  String KEY_FILE_LOCATION ;
	  private static  String SERVICE_ACCOUNT_EMAIL;
	 
	  Analytics analytics=null;
	  public GoogleAnalyticsClient(Configuration configuration) 
	  {
		  JSON_FACTORY = GsonFactory.getDefaultInstance();
		  		  APPLICATION_NAME=configuration.getAPPLICATION_NAME();
		  KEY_FILE_LOCATION=configuration.getKEY_FILE_LOCATION();
		  SERVICE_ACCOUNT_EMAIL=configuration.getSERVICE_ACCOUNT_EMAIL();
		  

	  }
	 
	  
	  public static Analytics initializeAnalytics() throws Exception 
	  {
		    // Initializes an authorized analytics service object.

		    // Construct a GoogleCredential object with the service account email
		    // and p12 file downloaded from the developer console.
		    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		    GoogleCredential credential = new GoogleCredential.Builder()
		        .setTransport(httpTransport)
		        .setJsonFactory(JSON_FACTORY)
		        .setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
		        .setServiceAccountPrivateKeyFromP12File(new File(KEY_FILE_LOCATION))
		        .setServiceAccountScopes(AnalyticsScopes.all())
		        .build();

		    // Construct the Analytics service object.
		    return new Analytics.Builder(httpTransport, JSON_FACTORY, credential)
		        .setApplicationName(APPLICATION_NAME).build();
		  }

	  
	  
	  public static MultiValueMap<String,Object> getFirstProfileId(Analytics analytics) throws IOException {
		    // Get the view (profile) ID for the authorized user.
		    String profileId = null;
		    org.apache.commons.collections4.map.MultiValueMap<String,Object> accountAndPropertyViewMap=new MultiValueMap<String, Object>();
		    
		   
		    // Query for the list of all accounts associated with the service account.
		    Accounts accounts = analytics.management().accounts().list().execute();

		    
		    if (accounts.getItems().isEmpty()) {
		      System.err.println("No accounts found");
		      SpringApp.googleAnalyticsLogger.log(Level.WARNING,"No accounts found");
		    } 
		    else
		    {
		    	int accountsCtr=0;
		    	
		    	
		    	while(accountsCtr < accounts.getItems().size())
		    	{
		    		
				    	
				      String firstAccountId = accounts.getItems().get(accountsCtr).getId();
				   System.out.println(">>>>>>>>>>>>>GA: account id:"+firstAccountId+">>>>GA:username:"+accounts.getUsername());
				     
				      Map<String,String> propertyViewMap=new HashMap<String, String>();
				      int propertyCtr=0;
				      
				      // Query for the list of properties associated with the  account.
				      Webproperties properties = analytics.management().webproperties()
				          .list(firstAccountId).execute();
		
				      if (properties.getItems().isEmpty()) 
				      {
				        System.err.println("No Webproperties found");
				        SpringApp.googleAnalyticsLogger.log(Level.WARNING,"No web properties found");
				      } else
				      {
				    	  while(propertyCtr < properties.getItems().size())
				    	  {
						        String firstWebpropertyId = properties.getItems().get(propertyCtr).getId();
			
						 	   System.out.println(">>>>>>>>>>>>>GA: property id:"+firstWebpropertyId);
								
						        // Query for the list views (profiles) associated with the property.
						        Profiles profiles = analytics.management().profiles()
						            .list(firstAccountId, firstWebpropertyId).execute();
				
						        if (profiles.getItems().isEmpty()) 
						        {
						          System.err.println("No views (profiles) found");
						          SpringApp.googleAnalyticsLogger.log(Level.WARNING,"No views/profiles found");
						        } 
						        else 
						        {
						        
						          // Return the first (view) profile associated with the property.
						          profileId = profiles.getItems().get(0).getId();
						          System.out.println(">>>>>>>>>>>>>GA: profile/view id:"+profileId);
									
						          propertyViewMap.put(firstWebpropertyId, profileId);
						          
						        }
						        
						        propertyCtr++;
						      }
				    	  accountAndPropertyViewMap.put(firstAccountId, propertyViewMap);
				      }
						      accountsCtr++;
				    }
		    }
		    return accountAndPropertyViewMap;
		  }

	  
	 public static GaData getTrafficSourcesData(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:referralPath,ga:campaign,ga:source,ga:medium")
		        .execute();
	  }

	  
	 public static GaData getPagePathByPageViews(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId, SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:pageViews").setDimensions("ga:date,ga:pagePath")
		        .execute();
	  }

	 public static GaData getPagePathByUniquePageViews(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:uniquePageViews").setDimensions("ga:date,ga:pagePath")
		        .execute();
	  }

	 public static GaData getPagePathByTimeOnPage(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:timeOnPage").setDimensions("ga:date,ga:pagePath")
		        .execute();
	  }

	 public static GaData getPagePathByAvgTimeOnPage(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:avgTimeOnPage").setDimensions("ga:date,ga:pagePath")
		        .execute();
	  }
	 public static GaData getPagePathByPageLoadTime(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:pageLoadTime").setDimensions("ga:date,ga:pagePath")
		        .execute();
	  }
	 public static GaData getPagePathByAvgPageLoadTime(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:avgPageLoadTime").setDimensions("ga:date,ga:pagePath")
		        .execute();
	  }
  
	 public static GaData getUsersByUserType(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:userType")
		        .execute();
	  }
	 public static GaData getNewUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:newUsers").setDimensions("ga:date")
		        .execute();
	  }

	 public static GaData getSessions(Analytics analytics, String profileId) throws IOException
	  {
		    // Query the Core Reporting API for the number of sessions
		    
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:sessions").setDimensions("ga:date")
		        .execute();
	  }

	 public static GaData getSessionsData(Analytics analytics, String profileId) throws IOException
	  {
		    // Query the Core Reporting API for the number of sessions + bounces+bounceRate
		    
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:sessions,ga:bounces,ga:bounceRate").setDimensions("ga:date")
		        .execute();
	  }
 
	 
	 public static GaData getBounces(Analytics analytics, String profileId) throws IOException
	  {
		    // Query the Core Reporting API for the number of bounces
		    
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:bounces").setDimensions("ga:date")
		        .execute();
	  }
	 public static GaData getBounceRate(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:bounceRate").setDimensions("ga:date")
		        .execute();
	  }
	 public static GaData getCampaignByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId, SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:campaign")
		        .execute();
	  }

	 public static GaData getReferralPathByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId, SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:referralPath")
		        .execute();
	  }
	 public static GaData getTrafficSources(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId, SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:referralPath,ga:campaign,ga:source,ga:medium")
		        .execute();
	  }
	 public static GaData getMediumByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    
		    
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:medium")
		        .execute();
	  }
	 public static GaData getSourceByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:source")
		        .execute();
	  }
	
	 public static GaData getPlatformOrDevice(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:mobileDeviceInfo,ga:deviceCategory,ga:browser,ga:browserVersion")
		        .execute();
	  }
	 
	 public static GaData getMobileDeviceInfoByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:mobileDeviceInfo")
		        .execute();
	  }
	 public static GaData getDeviceCategoryByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:deviceCategory")
		        .execute();
	  }
	 
	 public static GaData getBrowserByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:browser")
		        .execute();
	  }
	 public static GaData getBrowserVersionByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:browser,ga:browserVersion")
		        .execute();
	  }
	 public static GaData getCityByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:city")
		        .execute();
	  }
	  
	  public static GaData getCountryByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:country")
		        .execute();
	  }

	  public static GaData getGeoNetworkDataByUsers(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:city,ga:country")
		        .execute();
	  }
	  public static GaData getSiteSpeedData(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:pageLoadTime,ga:avgPageLoadTime").setDimensions("ga:date")
		        .execute();
	  }
	  public static GaData getPlatformData(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:mobileDeviceInfo,ga:browser,ga:browserVersion")
		        .execute();
	  }
	  public static GaData getDeviceData(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:users").setDimensions("ga:date,ga:deviceCategory")
		        .execute();
	  }
	  public static GaData getPageTrackingData(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:pageviews,ga:uniquePageviews,ga:timeOnPage,ga:avgTimeOnPage").setDimensions("ga:date,ga:pagePath")
		        .execute();
	  }
	  public static GaData getLandingPageData(Analytics analytics, String profileId) throws IOException
	  {
		    return analytics.data().ga()
		        .get("ga:" + profileId,SpringApp.configuration.getStartDate(),SpringApp.configuration.getEndDate(),"ga:newUsers,ga:sessions,ga:percentNewSessions,ga:bounceRate,ga:avgSessionDuration,ga:pageviewsPerSession").setDimensions("ga:date,ga:landingPagePath")
		        .execute();
	  }
}
