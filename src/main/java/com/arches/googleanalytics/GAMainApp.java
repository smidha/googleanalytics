package com.arches.googleanalytics;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class GAMainApp 
{
	
	private static final String CONFIG_PATH="application-config.xml";
	
	public static void main( String[] args ) throws Exception
    {
		
    	new ClassPathXmlApplicationContext(CONFIG_PATH);
		
    	
    }
}
