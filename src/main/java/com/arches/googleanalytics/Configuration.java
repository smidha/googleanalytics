package com.arches.googleanalytics;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import com.arches.model.Users;

public class Configuration
{
	private String APPLICATION_NAME;
	private String KEY_FILE_LOCATION;
	private String SERVICE_ACCOUNT_EMAIL;
	private String databaseDriver;
	private String databaseUrl;
	private String databaseUsername;
	private String databasePassword;
	private String databaseDB;
	private String databaseUsersTable;
	private String googleAnalyticsLogs;
	private String startDate;
	//private String startDateForSiteSpeed;
	private String endDate;
	private boolean stopApp;
	
	
	public boolean getStopApp() {
		return stopApp;
	}
	public void setStopApp(boolean stopApp) {
		this.stopApp = stopApp;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getGoogleAnalyticsLogs() {
		return googleAnalyticsLogs;
	}
	public void setGoogleAnalyticsLogs(String googleAnalyticsLogs) {
		this.googleAnalyticsLogs = googleAnalyticsLogs;
	}
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
/*	public String getStartDateForSiteSpeed() {
		return startDateForSiteSpeed;
	}
	public void setStartDateForSiteSpeed(String startDateForSiteSpeed) {
		this.startDateForSiteSpeed = startDateForSiteSpeed;
	}
	*/
	public String getDatabaseDriver() {
		return databaseDriver;
	}
	public void setDatabaseDriver(String databaseDriver) {
		this.databaseDriver = databaseDriver;
	}
	public String getDatabaseUrl() {
		return databaseUrl;
	}
	public void setDatabaseUrl(String databaseUrl) {
		this.databaseUrl = databaseUrl;
	}
	public String getDatabaseUsername() {
		return databaseUsername;
	}
	public void setDatabaseUsername(String databaseUsername) {
		this.databaseUsername = databaseUsername;
	}
	public String getDatabasePassword() {
		return databasePassword;
	}
	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}
	public String getDatabaseDB() {
		return databaseDB;
	}
	public void setDatabaseDB(String databaseDB) {
		this.databaseDB = databaseDB;
	}
	public String getDatabaseUsersTable() {
		return databaseUsersTable;
	}
	public void setDatabaseUsersTable(String databaseUsersTable) {
		this.databaseUsersTable = databaseUsersTable;
	}
	public String getAPPLICATION_NAME() {
		return APPLICATION_NAME;
	}
	public void setAPPLICATION_NAME(String aPPLICATION_NAME) {
		APPLICATION_NAME = aPPLICATION_NAME;
	}
	public String getKEY_FILE_LOCATION() {
		return KEY_FILE_LOCATION;
	}
	public void setKEY_FILE_LOCATION(String kEY_FILE_LOCATION) {
		KEY_FILE_LOCATION = kEY_FILE_LOCATION;
	}
	public String getSERVICE_ACCOUNT_EMAIL() {
		return SERVICE_ACCOUNT_EMAIL;
	}
	public void setSERVICE_ACCOUNT_EMAIL(String sERVICE_ACCOUNT_EMAIL) {
		SERVICE_ACCOUNT_EMAIL = sERVICE_ACCOUNT_EMAIL;
	}
	
}
