package com.arches.googleanalytics;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.collections4.map.MultiValueMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.arches.model.ApplicationContextProvider;
import com.arches.model.Device;
import com.arches.model.GeoNetwork;
import com.arches.model.LandingPage;
import com.arches.model.PageTracking;
import com.arches.model.Platform;
import com.arches.model.Sessions;
import com.arches.model.SiteSpeed;
import com.arches.model.Traffic;
import com.arches.model.Users;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analytics.Analytics;
import com.google.api.services.analytics.model.GaData;

public class SpringApp 
{

	//public static final String CONFIG_PATH = "application-config.xml";
	  private static String APPLICATION_NAME = null;
	  private static JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
	  private static String KEY_FILE_LOCATION = null;
	  private static String SERVICE_ACCOUNT_EMAIL = null;
	  private static GoogleAnalyticsClient googleAnalyticsClient=null;
	
	  private static ApplicationContext context = null;
	  public static Configuration configuration=null;
		public static Logger googleAnalyticsLogger;
	  public static DaoImpl daoImpl;
	  static String entryDate=null;
	  static String entryTZ=null;
	  static String actualDate=null;
	  static DateFormat dateFormat;
	  public static MultiValueMap<String,Object> accountAndPropertyViewMap;
	  
	
	  
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		 //context = new ClassPathXmlApplicationContext(CONFIG_PATH);
		context=ApplicationContextProvider.getApplicationContext();
		 configuration=(Configuration) context.getBean("configuration");
		 APPLICATION_NAME=configuration.getAPPLICATION_NAME();
		 KEY_FILE_LOCATION=configuration.getKEY_FILE_LOCATION();
		 SERVICE_ACCOUNT_EMAIL=configuration.getSERVICE_ACCOUNT_EMAIL();
		 googleAnalyticsLogger=Logger.getLogger("googleAnalyticsLogger");
		
		 FileHandler fileHandler;
 		try {
 			
 		
 			
 	  		fileHandler=new FileHandler(configuration.getGoogleAnalyticsLogs()+new SimpleDateFormat("MM-dd-yyyy").format(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime()).toString()+".txt",true);
 	  		googleAnalyticsLogger.addHandler(fileHandler);
 	  		SimpleFormatter simpleFormatter=new SimpleFormatter();
 	  		fileHandler.setFormatter(simpleFormatter);
 	  	   googleAnalyticsLogger.log(Level.INFO,"Google Analytics logging for:"+Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime());
 	  	}
 		catch(Exception e)
 		{
 			e.printStackTrace();
 		}
		 //initialize values for all DB logging
		 TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		   dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		  DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss Z");
		  Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		  entryDate=dateFormat.format(cal.getTime());
		  entryTZ=timeFormat.format(cal.getTime());
		  
		 googleAnalyticsClient=(GoogleAnalyticsClient)context.getBean("googleAnalyticsClient");
		 
	
    	 daoImpl=(DaoImpl) context.getBean("daoImpl");
		 
		 Analytics analytics = null;
    	 String profileId=null;

    	 String accountAndPropertyArray[]=null;
		try
		{
			analytics = googleAnalyticsClient.initializeAnalytics();
			//get multi valued map (1 account multiple properties)
			accountAndPropertyViewMap = googleAnalyticsClient.getFirstProfileId(analytics);
		
			
			for (int i=0;i<accountAndPropertyViewMap.size();i++)
			{
                List<Object> listOfGAaccounts = new ArrayList<Object>(accountAndPropertyViewMap.keySet());
                //i th GA account associated with this service account
                Object GAaccountID=listOfGAaccounts.get(i);
                System.out.println(">>>>>>>>>>>>>>>KEY: Account id:"+GAaccountID.toString());
                //get list of Maps of GAproperty<->View for the i th GAaccount ID(Its list because of multi valued map)
                Collection<Map<String,String>> propertyViewMapCollection=(List<Map<String,String>>)accountAndPropertyViewMap.get(GAaccountID.toString());
			

              Iterator listOfMapsIterator=propertyViewMapCollection.iterator();
              while(listOfMapsIterator.hasNext())
            	  
              {
            	
            	  
            	  		//next map in the list
            	  		Map<String,String> propertyViewMap=(Map<String, String>) listOfMapsIterator.next();
		                Iterator mapIterator = propertyViewMap.entrySet().iterator();
						while (mapIterator.hasNext()) 
						{
							//get pair of GA property and View
					        Map.Entry pair = (Map.Entry)mapIterator.next();
					       // 
					        //pair.getKey->GA property ID
					        System.out.println(">>>>>>>>>>>>>>>INNER KEY: property id:"+pair.getKey());
					        //pair.getValue()->GA view/profile ID
					        profileId=(String) pair.getValue();
					        System.out.println(">>>>>>>>>>>>>>>INNER VALUE: view id:"+pair.getValue());

					        
					        accountAndPropertyArray=daoImpl.getTableAccountIDAndPropertyName(GAaccountID.toString(),(String)pair.getKey());
					        if(accountAndPropertyArray==null)
					        {
					        	
					        	googleAnalyticsLogger.log(Level.SEVERE,"GA Account/property:"+GAaccountID.toString()+","+pair.getKey() +" returned by GA not found in data mart.");
					        	continue;
					        	
					        }
					        
					        
				 	       getAndSaveUsersData(analytics, profileId,accountAndPropertyArray);
					       getAndSaveSessionsData(analytics, profileId,accountAndPropertyArray);
					       getAndSaveTrafficSourcesData(analytics, profileId,accountAndPropertyArray);
					        getAndSaveGeoNetworkData(analytics, profileId, accountAndPropertyArray);
				           getAndSaveSiteSpeedData(analytics, profileId, accountAndPropertyArray);
					        getAndSavePlatformData(analytics, profileId, accountAndPropertyArray);
					        getAndSaveDeviceData(analytics, profileId, accountAndPropertyArray);
						    getAndSavePageTrackingData(analytics, profileId, accountAndPropertyArray);
					        getAndSaveLandingPageData(analytics, profileId, accountAndPropertyArray);
						}
              }
		}	
		}
			catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			googleAnalyticsLogger.log(Level.SEVERE,e.getMessage(),e);
		}
    	finally
    	{
    		for(Handler h:googleAnalyticsLogger.getHandlers())
    		{
    			System.out.println("closing log file...");
    		    h.close();   //must call h.close or a .LCK file will remain.
    		}
    	}
    
	     
	}
	
	
	private static void getAndSaveUsersData(Analytics analytics,String profileId,String accountAndPropertyArray[]) throws IOException, ParseException
	  {
		Users usersBean=null;
		String gaStringDate=null;
		Date gaDate;
		DateFormat gaDateFromFormat=new SimpleDateFormat("yyyyMMdd");
		DateFormat gaDateToFormat=new SimpleDateFormat("MM-dd-yyyy");
		//get users data
			GaData usersData=googleAnalyticsClient.getUsersByUserType(analytics, profileId);
	    	//one row expected with 2 cols(old and new users)
			 if (usersData!= null && usersData.getRows()!=null) 
			 {
				 //check if it has two cols
				  for(int j=0;j<usersData.getRows().size();j++)
		    	  {
					  usersBean=(Users) context.getBean("usersBean");
					  
					  
					  //formatting of GA date
					  gaStringDate=usersData.getRows().get(j).get(0);			  
					  gaDate=gaDateFromFormat.parse(gaStringDate);
					  gaStringDate=gaDateToFormat.format(gaDate);
					  //formatting completed
					  
		    		  usersBean.setUserType(usersData.getRows().get(j).get(1));
		    		  usersBean.setUsers(Double.parseDouble(usersData.getRows().get(j).get(2)));
		    		  usersBean.setActualDate(gaStringDate);
		    		  usersBean.setEntryDate(entryDate);
		    		  usersBean.setEntryTZ(entryTZ);
		    		 
		    		  usersBean.setAccountId(Long.parseLong(accountAndPropertyArray[0]));
		    		  usersBean.setProperty(Long.parseLong(accountAndPropertyArray[1]));
		    		  daoImpl.saveUsersData(usersBean);	
		    		  //daoImpl.saveUsersData(newUsersBean);
			  
		    	  }
			 }	  
			 else
			 {
				  System.out.println("error:userData either null/empty");
				  googleAnalyticsLogger.log(Level.INFO,"[SpringApp Analyzing GA Data]users data either null or empty with profileId:"+profileId);
			 }
	    	
	    }

	
	  private static void getAndSaveSessionsData(Analytics analytics,String profileId,String accountAndPropertyArray[]) throws IOException, ParseException
	  {
			GaData sessionsData=googleAnalyticsClient.getSessionsData(analytics, profileId);
		    	
	    	Sessions   sessions=(Sessions)context.getBean("sessions");
	    	Date gaDate;
			DateFormat gaDateFromFormat=new SimpleDateFormat("yyyyMMdd");
			DateFormat gaDateToFormat=new SimpleDateFormat("MM-dd-yyyy");
			String gaStringDate=null;
	    	
	    	 if (sessionsData!= null && sessionsData.getRows()!=null) 
			 {
	    		for(int j=0;j<sessionsData.getRows().size();j++) 
				   	 {
					  
					  //formatting of GA date
					  gaStringDate=sessionsData.getRows().get(j).get(0);			  
					  gaDate=gaDateFromFormat.parse(gaStringDate);
					  gaStringDate=gaDateToFormat.format(gaDate);
					  //formatting completed
					  	
					  	  sessions.setActualDate(gaStringDate);
						  sessions.setSessions(Double.parseDouble(sessionsData.getRows().get(j).get(1)));
						  sessions.setBounces(Double.parseDouble(sessionsData.getRows().get(j).get(2)));
						  sessions.setBounceRate(Double.parseDouble(sessionsData.getRows().get(j).get(3)));
						  sessions.setEntryDate(entryDate);
					    	 sessions.setEntryTZ(entryTZ);
					    	 
					    	 sessions.setAccountId(Long.parseLong(accountAndPropertyArray[0]));
					    	 sessions.setProperty(Long.parseLong(accountAndPropertyArray[1]));
					    	 System.out.println("---------DAO:Saving sessions data-----------");
						  daoImpl.saveSessionsData(sessions);
			    	  }
			 }
	    	 else
	    	 {
	    		 System.out.println("error:sessionsData null/empty with profileId:"+profileId);
	    		  googleAnalyticsLogger.log(Level.INFO,"[SpringApp Analyzing GA Data]sessions data nul or empty");
	    	 }
	    	
	    	
	    	 
	    	
	    	
	    
	  }	
	
	
	  
		private static void getAndSaveTrafficSourcesData(Analytics analytics,String profileId,String accountAndPropertyArray[]) throws IOException, ParseException
		  {
			
			Traffic traffic=(Traffic) context.getBean("traffic");
			Date gaDate;
			DateFormat gaDateFromFormat=new SimpleDateFormat("yyyyMMdd");
			DateFormat gaDateToFormat=new SimpleDateFormat("MM-dd-yyyy");
			String gaStringDate;
	    
			//get users data
				GaData trafficData=googleAnalyticsClient.getTrafficSourcesData(analytics, profileId);
		    //	GaData 	newUsersData=googleAnalyticsClient.getNewUsers(analytics, profileId);
				//one row expected with 2 cols(old and new users)
				 if (trafficData!= null && trafficData.getRows()!=null) 
				 {
					 
					  for(int j=0;j<trafficData.getRows().size();j++)
			    	  {
						 
						  if(trafficData.getRows().get(j).size()<5)
						  {
							  googleAnalyticsLogger.log(Level.WARNING,"Traffic Data contains insufficient columns. Expected:5 columns. Found:"+trafficData.getRows().get(j).size());
							  continue;
						  }
						  //formatting of GA date
						  gaStringDate=trafficData.getRows().get(j).get(0);			  
						  gaDate=gaDateFromFormat.parse(gaStringDate);
						  gaStringDate=gaDateToFormat.format(gaDate);
						  //formatting completed
				
						  traffic.setActualDate(gaStringDate);
						  traffic.setReferralPath(trafficData.getRows().get(j).get(1));
						  traffic.setCampaign(trafficData.getRows().get(j).get(2));
						  traffic.setSource(trafficData.getRows().get(j).get(3));
						  traffic.setMedium(trafficData.getRows().get(j).get(4));
						  traffic.setUsers(Double.parseDouble(trafficData.getRows().get(j).get(5)));
						  traffic.setEntryDate(entryDate);
						  traffic.setEntryTZ(entryTZ);
						  	 
						  	 traffic.setAccountId(Long.parseLong(accountAndPropertyArray[0]));
						  	 traffic.setProperty(Long.parseLong(accountAndPropertyArray[1]));
							 System.out.println("---------DAO:Saving traffic data-----------");
							    
						  	 daoImpl.saveTrafficData(traffic);
			    	  }
					
				 }
		    	 else
		    	 {
		    		 System.out.println("error:traffic Data null/empty");
		    		  googleAnalyticsLogger.log(Level.INFO,"[SpringApp Analyzing GA Data]Traffic data null or empty");
		    	 }
		  }
		
		private static void getAndSaveGeoNetworkData(Analytics analytics,String profileId,String accountAndPropertyArray[]) throws IOException, ParseException
		  {
			Date gaDate;
			DateFormat gaDateFromFormat=new SimpleDateFormat("yyyyMMdd");
			DateFormat gaDateToFormat=new SimpleDateFormat("MM-dd-yyyy");
			String gaStringDate;
	    
			GeoNetwork geoNetwork=(GeoNetwork) context.getBean("geoNetwork");
			//get users data
				GaData geoNetworkData=googleAnalyticsClient.getGeoNetworkDataByUsers(analytics, profileId);
				 if (geoNetworkData!= null && geoNetworkData.getRows()!=null) 
				 {
					 
					  for(int j=0;j<geoNetworkData.getRows().size();j++)
			    	 {
						  //formatting of GA date
						  gaStringDate=geoNetworkData.getRows().get(j).get(0);			  
						  gaDate=gaDateFromFormat.parse(gaStringDate);
						  gaStringDate=gaDateToFormat.format(gaDate);
						  //formatting completed
				
						  geoNetwork.setCity(geoNetworkData.getRows().get(j).get(1));
						  geoNetwork.setCountry(geoNetworkData.getRows().get(j).get(2));
						  geoNetwork.setUsers(Long.parseLong(geoNetworkData.getRows().get(j).get(3)));
						  geoNetwork.setEntryDate(entryDate);
						  geoNetwork.setEntryTZ(entryTZ);
						  geoNetwork.setActualDate(gaStringDate);
						  geoNetwork.setAccountId(Long.parseLong(accountAndPropertyArray[0]));
						  geoNetwork.setProperty(Long.parseLong(accountAndPropertyArray[1]));
							 System.out.println("---------DAO:Saving Geo Network data-----------");
							    
						  daoImpl.saveGeoNetworkData(geoNetwork);
			    	 }
					 
				 }
				 else
				 {
					 googleAnalyticsLogger.log(Level.INFO,"[SpringApp Analyzing GA Data]no geo networking data found  with profileId:"+profileId);
				 }
					 
		  
		  }
	
		
		
		private static void getAndSaveSiteSpeedData(Analytics analytics,String profileId,String accountAndPropertyArray[]) throws IOException, ParseException
		  {
			Date gaDate;
			DateFormat gaDateFromFormat=new SimpleDateFormat("yyyyMMdd");
			DateFormat gaDateToFormat=new SimpleDateFormat("MM-dd-yyyy");
			String gaStringDate;

			SiteSpeed siteSpeed=(SiteSpeed) context.getBean("siteSpeed");
			//get users data
				GaData siteSpeedData=googleAnalyticsClient.getSiteSpeedData(analytics, profileId);
				 if (siteSpeedData!= null && siteSpeedData.getRows()!=null) 
				 {
					 
					  for(int j=0;j<siteSpeedData.getRows().size();j++)
			    	 {
						  //formatting of GA date
						  gaStringDate=siteSpeedData.getRows().get(j).get(0);			  
						  gaDate=gaDateFromFormat.parse(gaStringDate);
						  gaStringDate=gaDateToFormat.format(gaDate);
						  //formatting completed
				
						  siteSpeed.setPageLoadTime(Double.parseDouble(siteSpeedData.getRows().get(j).get(1)));
						  siteSpeed.setAvgPageLoadTime(Double.parseDouble(siteSpeedData.getRows().get(j).get(2)));
						  siteSpeed.setEntryDate(entryDate);
						  siteSpeed.setEntryTZ(entryTZ);
						  siteSpeed.setActualDate(gaStringDate);
						  siteSpeed.setAccountId(Long.parseLong(accountAndPropertyArray[0]));
						  siteSpeed.setProperty(Long.parseLong(accountAndPropertyArray[1]));
							 System.out.println("---------DAO:Saving site speed data-----------");
							    
						  daoImpl.saveSiteSpeedData(siteSpeed);			    	 
						  }
					 
				 }
				 else
				 {
					 googleAnalyticsLogger.log(Level.INFO,"[SpringApp Analyzing GA Data]no site speed data found  with profileId:"+profileId);
				 }
		  
		  }
	
		
		private static void getAndSavePlatformData(Analytics analytics,String profileId,String accountAndPropertyArray[]) throws IOException, ParseException
		  {
			Date gaDate;
			DateFormat gaDateFromFormat=new SimpleDateFormat("yyyyMMdd");
			DateFormat gaDateToFormat=new SimpleDateFormat("MM-dd-yyyy");
			String gaStringDate;

			Platform platform=(Platform) context.getBean("platform");
			//get users data
				GaData platformData=googleAnalyticsClient.getPlatformData(analytics, profileId);
				 if (platformData!= null && platformData.getRows()!=null) 
				 {
					 
					  for(int j=0;j<platformData.getRows().size();j++)
			    	 {
						  //formatting of GA date
						  gaStringDate=platformData.getRows().get(j).get(0);			  
						  gaDate=gaDateFromFormat.parse(gaStringDate);
						  gaStringDate=gaDateToFormat.format(gaDate);
						  //formatting completed

						  platform.setMobileDeviceInfo(platformData.getRows().get(j).get(1));
						  platform.setBrowser(platformData.getRows().get(j).get(2));
						  platform.setBrowserVersion(platformData.getRows().get(j).get(3));
						  platform.setUsers(Double.parseDouble(platformData.getRows().get(j).get(4))); 
						  platform.setEntryDate(entryDate);
						  platform.setEntryTZ(entryTZ);
						  platform.setActualDate(gaStringDate);
						  platform.setAccountId(Long.parseLong(accountAndPropertyArray[0]));
						  platform.setProperty(Long.parseLong(accountAndPropertyArray[1]));
							 System.out.println("---------DAO:Saving platform data-----------");
							    
						  daoImpl.savePlatformData(platform);			    	 }
					 
				 }
				 else
				 {
					 googleAnalyticsLogger.log(Level.INFO,"[SpringApp Analyzing GA Data]No platform data found with profileId:"+profileId);
				 }
		  
		  }
	
		
		private static void getAndSaveDeviceData(Analytics analytics,String profileId,String accountAndPropertyArray[]) throws IOException, ParseException
		  {
			Date gaDate;
			DateFormat gaDateFromFormat=new SimpleDateFormat("yyyyMMdd");
			DateFormat gaDateToFormat=new SimpleDateFormat("MM-dd-yyyy");
			String gaStringDate;

			Device device=(Device) context.getBean("device");
			//get users data
				GaData deviceData=googleAnalyticsClient.getDeviceData(analytics, profileId);
				 if (deviceData!= null) 
				 {
					 if(deviceData.getRows()!=null)
					 {
						 for(int j=0;j<deviceData.getRows().size();j++)
						 {
						  //formatting of GA date for DB storage
						  gaStringDate=deviceData.getRows().get(j).get(0);			  
						  gaDate=gaDateFromFormat.parse(gaStringDate);
						  gaStringDate=gaDateToFormat.format(gaDate);
						  //formatting completed

						  device.setDeviceCategory(deviceData.getRows().get(j).get(1));
						  device.setUsers(Double.parseDouble(deviceData.getRows().get(j).get(2)));
						  device.setEntryDate(entryDate);
						  device.setEntryTZ(entryTZ);
						  device.setActualDate(gaStringDate);
						  device.setAccountId(Long.parseLong(accountAndPropertyArray[0]));
						  device.setProperty(Long.parseLong(accountAndPropertyArray[1]));
							 System.out.println("---------DAO:Saving device data-----------");
							    
						  daoImpl.saveDeviceData(device);
						  }
					 }
					 else
					 {
						 googleAnalyticsLogger.log(Level.INFO,"[SpringApp Analyzing GA Data]no rows found for device data with profileId:"+profileId);
					 }
				 }
				 else
				 {
					 googleAnalyticsLogger.log(Level.INFO,"[SpringApp Analyzing GA Data]no device data found with profileId:"+profileId);
				 }
		  
		  }
		private static void getAndSavePageTrackingData(Analytics analytics,String profileId,String accountAndPropertyArray[]) throws IOException, ParseException
		  {
			Date gaDate;
			DateFormat gaDateFromFormat=new SimpleDateFormat("yyyyMMdd");
			DateFormat gaDateToFormat=new SimpleDateFormat("MM-dd-yyyy");
			String gaStringDate;

			PageTracking pageTracking=(PageTracking) context.getBean("pageTracking");
			//get users data
				GaData pageData=googleAnalyticsClient.getPageTrackingData(analytics, profileId);
				 if (pageData!= null && pageData.getRows()!=null) 
				 {
					 
					  for(int j=0;j<pageData.getRows().size();j++)
			    	 {
						  //formatting of GA date for DB storage
						  gaStringDate=pageData.getRows().get(j).get(0);			  
						  gaDate=gaDateFromFormat.parse(gaStringDate);
						  gaStringDate=gaDateToFormat.format(gaDate);
						  //formatting completed

						  pageTracking.setPagePath(pageData.getRows().get(j).get(1));
						  pageTracking.setPageViews(Double.parseDouble(pageData.getRows().get(j).get(2)));
						  pageTracking.setUniquePageViews(Double.parseDouble(pageData.getRows().get(j).get(3)));
						  pageTracking.setTimeOnPage(Double.parseDouble(pageData.getRows().get(j).get(4)));
						  pageTracking.setAvgTimeOnPage(Double.parseDouble(pageData.getRows().get(j).get(5)));
						  pageTracking.setActualDate(gaStringDate);
						  pageTracking.setEntryDate(entryDate);
						  pageTracking.setEntryTZ(entryTZ);
						  pageTracking.setAccountId(Long.parseLong(accountAndPropertyArray[0]));
						  pageTracking.setProperty(Long.parseLong(accountAndPropertyArray[1]));
							 System.out.println("---------DAO:Saving page tracking data-----------");
							    
						  daoImpl.savePageTrackingData(pageTracking);
						  }
					 
				 }

				 else
				 {
					 googleAnalyticsLogger.log(Level.INFO,"[SpringApp Analyzing GA Data]no page data with profileId:"+profileId);
				 }
		  }
	
		private static void getAndSaveLandingPageData(Analytics analytics,String profileId,String accountAndPropertyArray[]) throws IOException, ParseException
		  {
			Date gaDate;
			DateFormat gaDateFromFormat=new SimpleDateFormat("yyyyMMdd");
			DateFormat gaDateToFormat=new SimpleDateFormat("MM-dd-yyyy");
			String gaStringDate;

			LandingPage landingPage=(LandingPage) context.getBean("landingPage");
			//get users data
				GaData pageData=googleAnalyticsClient.getLandingPageData(analytics, profileId);
				 if (pageData!= null && pageData.getRows()!=null) 
				 {
					 
					  for(int j=0;j<pageData.getRows().size();j++)
			    	 {		  //formatting of GA date for DB storage
						  gaStringDate=pageData.getRows().get(j).get(0);			  
						  gaDate=gaDateFromFormat.parse(gaStringDate);
						  gaStringDate=gaDateToFormat.format(gaDate);
						  //formatting completed

						  landingPage.setLandingPagePath(pageData.getRows().get(j).get(1));
						  landingPage.setNewUsers(Double.parseDouble(pageData.getRows().get(j).get(2)));
						  landingPage.setSessions(Double.parseDouble(pageData.getRows().get(j).get(3)));
						  landingPage.setPercentNewSessions(Double.parseDouble(pageData.getRows().get(j).get(4)));
						  landingPage.setBounceRate(Double.parseDouble(pageData.getRows().get(j).get(5)));
						  landingPage.setAvgSessionDuration(Double.parseDouble(pageData.getRows().get(j).get(6)));
						  landingPage.setPageViewsPerSession(Double.parseDouble(pageData.getRows().get(j).get(7)));
						  landingPage.setEntryDate(entryDate);
						  landingPage.setEntryTZ(entryTZ);
						  landingPage.setActualDate(gaStringDate);
						  landingPage.setAccountId(Long.parseLong(accountAndPropertyArray[0]));
						  landingPage.setProperty(Long.parseLong(accountAndPropertyArray[1]));
					      System.out.println("---------DAO:Saving landing page data-----------");
							    
						  daoImpl.saveLandingPageData(landingPage);
			    	 }
				 }
				 else
				 {
					 googleAnalyticsLogger.log(Level.INFO,"[SpringApp Analyzing GA Data]no landing page data found  with profileId:"+profileId);
				 }
		  }
		
		
		private static void printResults(GaData results) {
		    // Parse the response from the Core Reporting API for
		    // the profile name and number of sessions.
		    if (results != null && !results.getRows().isEmpty() ) {
		      System.out.println("View (Profile) Name: "
		        + results.getProfileInfo().getProfileName());
		      System.out.println("prpfile Imfp: "
				        + results.getProfileInfo().getAccountId()+"#"+results.getProfileInfo().getProfileId());
				     
		      
		      System.out.println("result size:"+results.getTotalResults());
		     // System.out.println("cat1: " + results.getRows().get(0).get(0));
		     // System.out.println("Total users: " + results.getRows().get(0).get(1));
		     // System.out.println("cat2: " + results.getRows().get(1).get(0));
		      //System.out.println("Total users: " + results.getRows().get(1).get(1));
		     // System.out.println("cat3: " + results.getRows().get(2).get(0));
		     // System.out.println("Total users: " + results.getRows().get(2).get(1));
		   int i=0;
		      while(i>=0)
		      {
		    	  
		    	  System.out.print("data:"+results.getRows().get(i).get(0)+"   ");
		    	  if(results.getRows().get(i).size()==2)
		    	  {
		    		  System.out.println(results.getRows().get(i).get(1));
		    		  
		    	  }
		    	  if(results.getRows().get(i).size()==3)
		    	  {
		    		  System.out.print(results.getRows().get(i).get(1));
		    		  System.out.println("   "+results.getRows().get(i).get(2));
		    		  
		    	  }
		    	  if(results.getRows().get(i).size()==4)
		    	  {
		    		  System.out.print(results.getRows().get(i).get(1));
		    		  System.out.println("   "+results.getRows().get(i).get(2));
		    		  System.out.println("   "+results.getRows().get(i).get(3));
		  	    		  
		    	  }
		    	  if(results.getRows().get(i).size()==5)
		    	  {
		    		  System.out.print(results.getRows().get(i).get(1));
		    		  System.out.println("   "+results.getRows().get(i).get(2));
		    		  System.out.println("   "+results.getRows().get(i).get(3));
		    		  System.out.println("   "+results.getRows().get(i).get(4));
		    		 
		  	  	    		  
		    	  }
		    	  if(i==results.getTotalResults()-1)
		    	  {
		    		  break;
		    	  }
		    	  i++;
		      }
		      
		    } else {
		      System.out.println("No results found");
		    }
		  }
		  
		 

}